from flask import Flask, request

app = Flask(__name__)

tasks = {
    1: {
        'descricao': 'Jogar BF5',
        'prioridade': 'ALTA'
    },
    2: {
        'descricao': 'estudar docker',
        'prioridade': 'BAIXA'
    },
    3: {
        'descricao': 'ir pra academia',
        'prioridade': 'MEDIA'
    },
    4: {
        'descricao': 'ler a biblia',
        'prioridade': 'BAIXA'
    },
    5: {
        'descricao': 'estudar jenkins',
        'prioridade': 'ALTA'
    }
}


@app.route('/api/v1.0/tasks')
def get_tasks():
    prioridade = request.args.get('prioridade')
    filtered_tasks = filter_tasks(prioridade) if prioridade != None else tasks
    return filtered_tasks


@app.route('/api/v1.0/tasks', methods=["POST"])
def create_task():
    task = request.get_json(force=True)
    tasks[len(tasks) + 1] = task
    return tasks


@app.route('/api/v1.0/tasks/<id>')
def get_product(id):
    task = tasks[int(id)] if int(id) in tasks.keys() else {
        'error': 'NOT FOUND', 'status_code': 404}
    return task


@app.route('/api/v1.0/tasks/<id>')
def update_task(id):
    task = get_task(id)
    if 'error' in task.keys():
        return task

    updated_task = request.get_json(force=True)
    tasks[int(id)] = updated_task
    return tasks


def filter_tasks(prioridade):
    return list(filter(lambda x: x['prioridade'] == prioridade, tasks.values()))


def get_task(id):
    task = tasks[int(id)] if int(id) in tasks.keys() else {
        'error': 'NOT FOUND', 'status_code': 404}
    return task


if __name__ == '__main__':
    app.run(debug=True)
