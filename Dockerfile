FROM python:3-alpine

COPY ./requirements.txt /root/requirements.txt

WORKDIR /root

# RUN \
#   apk add --no-cache postgresql-libs && \
#   apk add --no-cache --virtual .build-deps gcc musl-dev postgresql-dev && \
#   python3 -m pip install -r requirements.txt --no-cache-dir && \
#   python3 -m pip install pytest && \
#   apk --purge del .build-deps

RUN pip install -r requirements.txt

COPY app.py /root/app.py

ENV FLASK_APP=app.py
ENV FLASK_ENV=development

CMD ["python", "-m", "flask", "run"]
